#ifndef INPUTLETTER_H
#define INPUTLETTER_H

#include <QLabel>

typedef enum {
    INPUT_LETTER, // the letter is looking for a letter
    GREEN_LETTER, // the letter is in the right place
    GREY_LETTER,  // the letter isn't in the word
    YELLOW_LETTER // the letter is in the word but not in this place
} state_input_letter;


/**
 * @brief The InputLetter class
 *
 * @author Antoine Haslé
 *
 * @param sil the state of letter
 * @param value the letter in the QTextEdit
 * @param cbg the color of the background
 * @param ctext color of the text
 */
class InputLetter: public QLabel
{
    Q_OBJECT
public:
    /**
     * @brief Construct a new Input Letter object
     *
     * @param parent the parent widget
     * @param sil the state of letter
     * @param v a pointer to the char in the text edit
     */
    InputLetter(QWidget* parent = nullptr, state_input_letter sil = INPUT_LETTER, QChar* v = nullptr);

    /**
     * @brief Destroy the Input Letter object
     */
    ~InputLetter();

    /**
     * @brief getLetter
     * @return
     */
    QChar* getLetter();


    state_input_letter getSil() const;
    static int silToInt(QVector<state_input_letter> sil);
private:
    /**
     * @brief Get the Back Ground Color object
     *
     * @param nsil new state input letter
     * @return QColor* pointer to the color
     */
    static QColor* getBackGroundColor(state_input_letter& nsil);

    /**
     * @brief update the background color and the color of the text
     */
    void updateColor();

private:
    state_input_letter sil;
    QChar* value;
    QColor* cbg;
    QColor* ctext;

public slots:
    /**
     * @brief Set the State Input Letter object
     */
    void setStateInputLetter(const state_input_letter);

    /**
     * @brief setLetter
     * @param c valid char and between A and Z
     */
    void setLetter(const QChar& c);
};

#endif // INPUTLETTER_H
