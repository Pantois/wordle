#ifndef INPUTWORD_H
#define INPUTWORD_H

#include <QWidget>
#include <QHBoxLayout>
#include <inputletter.h>

typedef enum {DONE, IN_PROGRESS, WAITING} state_input_word;

class InputWord : public QWidget
{
    Q_OBJECT
private:
    size_t sizeWord;
    state_input_word wordState;
    QHBoxLayout* layout;
    int currentLetter;
    QVector<InputLetter*>* letters;
public:
    /**
     * @brief InputWord
     * @param parent
     * @param s
     * @param wordState
     */
    InputWord(QWidget *parent = nullptr, const size_t s = 5, state_input_word wordState = WAITING);



    ~InputWord();
    /**
     * @brief getWord
     * @return
     */
    QString getWord();

    /**
     * @brief isComplete
     * @return
     */
    bool isComplete();

    /**
     * @brief setstate_input_word
     * @param siw
     */
    void setstate_input_word(state_input_word siw);

    /**
     * @brief getStateInputLetters
     * @return the states of the letters in this word
     */
    QVector<state_input_letter> getStateInputLetters();
public slots:

    /**
     * @brief showAnswer
     * @param sil
     */
    void showAnswer(QVector<state_input_letter>* sil);

    /**
     * @brief letterEntry
     * @param a letter in input between A and Z
     */
    void letterEntry(const QChar);

    /**
     * @brief remove a letter in the word
     * 
     */
    void removeLetter();


};

#endif // INPUTWORD_H
