#ifndef WORDLE_H
#define WORDLE_H

#include <QWidget>
#include <QSettings>
#include <inputword.h>
#include <QVBoxLayout>
#include <QKeyEvent>
#include <wordreader.h>
#include <QPushButton>
#include <QListView>

#include <suggestionlistmodel.h>

/**
 * @brief Wordle class contains 
 * 
 * @param inputWords, QVector<InputWord*>* represents a line of the wordle
 * @param layout, QVBoxLayout* represent the layout of this class
 * @param currentWord, unsigned int, inputWords->at(currentWord) is the word in which the user can write on.
 * @param sizeWord, size_t, the size of a word
 * @param wr, WordReader*
 * @param secretWord, QString, the Answer
 * @param label, QLabel*, the label in which the user can see the answer after the game
 * @param buttonRetry, QPushButton*
 * @param buttonNewGame, QPushButton* 
 * @param settings, QSettings
 * @param lstPlayedWord, QVector<QString> of played words
 * @param lstAnswer, QVector<QVector<state_input_letter>> states of the played words
 * @param lstLastWord, QVector<QString*>* contains the possible words after answer of the player
 * @param lstGreyChar, QVector<bool> lstGreyChar.at(c-'A') is true if the c the char is not in the word
 *
 * @param nb_word, int, the number of words in the wordle
 */
class Wordle : public QWidget
{
    Q_OBJECT
private:
    static const size_t nb_word = 7;
    QVector<InputWord*>* inputWords;
    QVBoxLayout* layout;
    unsigned int currentWord;
    bool suggestion;
    size_t sizeWord;
    WordReader* wr;
    QString secretWord;
    QLabel* label;
    QPushButton* buttonRetry;
    QPushButton* buttonNewGame;
    SuggestionListModel* model;
    QListView* view;
    QVector<QString> lstPlayedWord;
    QVector<QVector<state_input_letter>> lstAnswer;
    QVector<QString*>* lstLastWord;
    QVector<bool> lstGreyChar;
    QVector<QChar> lstGreenChar;
    QVector<bool> lstYellow;
    QVector<double> lstFreq;
public:
    /**
     * @brief Wordle
     * @param parent
     * @param sizeWord
     * @param l
     */
    explicit Wordle(QWidget *parent = nullptr, size_t sizeWord = 5, bool suggestions = false, QLocale* l = nullptr);

    /**
     * @brief Destroy the Wordle object
     * 
     */
    ~Wordle();

    /**
     * @brief isLastWord 
     * 
     * @return true if currentWord == lastWord-1
     * @return false in others cases
     */
    bool isLastWord();

    /**
     * @brief getAnswer
     * 
     * @return QVector<state_input_letter>* which contains the colors the answer should be
     */
    QVector<state_input_letter>* getAnswer(QString value = QString(""), QString strSecretWord = QString(""));

    /**
     * @brief show to user if he won or not and the secret word, and display replay buttons
     * 
     * @param success true if the user won
     */
    void showAnswer(bool success);

    /**
     * @brief Get the Number Availlable Word object
     * 
     * @return size_t 
     */
    size_t getNumberAvaillableWord();

    /**
     * @brief Get the Suggest Word object
     * 
     * @return QString 
     */
    void getSuggestWord();

    /**
     * @brief Get the Suggest Word object
     * 
     * @param str which str.length < sizeWord
     * @return QString begining of the word
     */
    void getSuggestWord(QString str);

    /**
     * @brief manage the keyPressEvent
     * 
     * @param event 
     */
    void keyPressEvent(QKeyEvent* event);
private:
    bool verifyFilterNaive(QString& str, QVector<state_input_letter>& f, QString& vRes);
    /**
     * @brief verifyFilter
     * @param str
     * @param f
     * @param vRes
     * @return
     * @warning not working !
     */
    bool verifyFilter(QString& str, QVector<state_input_letter>& f, QString& vRes);
    int getWordScoreToFind(const QString& str);
    /**
     * @brief test if str is a word
     * 
     * @param str 
     * @return true if str is a real word
     */
    bool isWord(QString & str);

    /**
     * @brief choose a random word in thee commons word
     * 
     * @param s 
     * @return QString 
     */
    QString generateSecretWord(size_t s);

    static int numberSimilarLetter(const QString &str);
    static QVector<double> calcLstFreq(QVector<QString*>* lstWord);
public slots:

    /**
     * @brief slot connected to the signal of the @a buttonRetry
     * 
     */
    void retry();

    /**
     * @brief slot connected to the signal of the @a buttonNewGame
     * 
     */
    void newGame();
signals:
    /**
     * @brief emited after an accepted submition
     * 
     */
    void updateStatusBar(QString);

    /**
     * @brief emited after an accepted submition
     * 
     */
    void sendAnswer(QVector<state_input_letter>*);

    /**
     * @brief emited when the user presses a letter
     * 
     */
    void addLetter(const QChar);

    /**
     * @brief emited when the user presses on remove key
     * 
     */
    void removeLetter();

    /**
     * @brief emited when the user push a new game button
     * 
     * @param sameProperties true if it's the retry button, false otherwise
     */
    void newGame(bool sameProperties);
};

#endif // WORDLE_H
