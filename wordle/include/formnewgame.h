#ifndef FORMNEWGAME_H
#define FORMNEWGAME_H

#include <QWidget>

namespace Ui {
class FormNewGame;
}

class FormNewGame : public QWidget
{
    Q_OBJECT

public:
    explicit FormNewGame(QWidget *parent = nullptr, size_t s = 5, bool suggestions = false);
    ~FormNewGame();

private slots:
    void on_pushButton_clicked();

    void on_horizontalSlider_actionTriggered(int action);

    void on_checkBox_stateChanged(int arg1);
signals:
    void newGame(int,bool);
private:
    Ui::FormNewGame *ui;
    size_t length;
    bool suggestions;
};

#endif // FORMNEWGAME_H
