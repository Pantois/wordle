#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTranslator>
#include <wordle.h>
#include <formnewgame.h>
#include <appsettings.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, QApplication* app = nullptr);
    ~MainWindow();
protected:
    void keyPressEvent(QKeyEvent* event);
private slots:
    void on_actionNouvelle_partie_triggered();

    void on_actionVoir_la_r_ponse_triggered();

    void on_actionQuitter_triggered();

    void on_actionFran_ais_triggered();

    void on_actionAnglais_triggered();

    void on_actionA_propos_triggered();

    void statusBar(QString);

    void newGame(int, bool);

    void newSetUp(bool);
signals:
    void languageChanged(const QLocale & locale);
private:
    Wordle* wordle;
    AppSettings *settings;
    FormNewGame* fng;
    Ui::MainWindow *ui;

};
#endif // MAINWINDOW_H
