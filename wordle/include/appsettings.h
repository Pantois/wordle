#ifndef AppSettings_H
#define AppSettings_H

#include <QObject>
#include <QApplication>
#include <QSettings>
#include <QTranslator>



class AppSettings : public QObject
{
    Q_OBJECT
private:
    static const QLocale defaultLocale;
    static const QString languageKey;
    static const QString defaultSizeKey;
    static const size_t defaultSize;
    static const QString suggestionKey;
    static const bool suggestionDefault;

    /**
     * Application name
     */
    const QString applicationName;
    QSettings settings;
    QTranslator translator;
    QLocale* language;
    size_t length_default;
    bool suggestions;
    QApplication * app;

public:
    explicit AppSettings(QApplication *app);
    ~AppSettings();

    QLocale getLanguage() const;

    size_t getLength_default() const;
    void setLength_default(size_t newLength_default);

    const QTranslator &getTranslator() const;

    QApplication *getApp() const;

    bool getSuggestions() const;
    void setSuggestions(bool newSuggestions);

public slots:
    void setLanguage(const QLocale & language);
signals:

};

#endif // AppSettings_H
