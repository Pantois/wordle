#ifndef SUGGESTIONLISTMODEL_H
#define SUGGESTIONLISTMODEL_H

#include <QAbstractListModel>

typedef enum {DISPLAY_ROLE, ELIMINATION_ROLE} CustomDataRole;

class SuggestionListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit SuggestionListModel(QObject *parent = nullptr);


    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void insertAt(int row, QPair<QString, int>& value);
    void insert(QPair<QString, int>& value);
    void reset(bool resetAll = false);
    void displayBegin(QString& str, int sizeWord);
signals:
    void rowsInserted(const QModelIndex &parent, int first, int last);
    void rowsRemoved(const QModelIndex &parent, int first, int last);
private:
    QList<QPair<QString, int>*>* lst;
    QList<QPair<QString, int>*>* lstAll;
    QString prefix;
};

#endif // SUGGESTIONLISTMODEL_H
