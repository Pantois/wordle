#ifndef WORDREADER_H
#define WORDREADER_H

#include <QFile>

typedef enum {
    SCRABLE, COMMON
} type_file;


/**
 * @brief The WordReader class is the class which reads files which contains words
 *
 * @param lstWordCommon,  QVector<QString*>*
 * @param lstWordScrable,  QVector<QString*>*
 * @param f_scrable, QFile*
 * @param f_common, QFile*
 */
class WordReader : public QObject
{
public:
    /**
     * @brief WordReader constructor to read files which contains the words of size s
     * @param s
     */
    WordReader(size_t s, QLocale* l);

    /**
     * @brief ~WordReader destroy a WordReader
     */
    ~WordReader();

    /**
     * @brief WordInFile test if str is in @a lstWordScrable
     * @param str
     * @return
     */
    bool WordInFile(QString str);

    /**
     * @brief randWord
     * @return QString a random word in lstWordCommon
     */
    QString randWord();


    /**
     * @brief nameFile
     * @param tf
     * @param s
     * @return QString return the name of the file which contains the words of type s and the type Scrable / Common
     */
    static QString nameFile(type_file tf, size_t s, QLocale* l);

    /**
     * @brief getExtractWordFromFileScrable
     * @return lstWordScrable
     */
    QVector<QString*>* getExtractWordFromFileScrable();
private:

    /**
     * @brief extractWordFromFile put the word in @a f in @a lst
     * @param f
     * @param lst
     */
    static void extractWordFromFile(QFile* f, QVector<QString*>* lst);

    /**
     * @brief extractWordFromFileScrable create and return lstWordScrable
     * @return lstWordScrable
     */
    QVector<QString*>* extractWordFromFileScrable();

    /**
     * @brief extractWordFromFileCommon create and return lstWordCommon
     * @return lst
     */
    QVector<QString*>* extractWordFromFileCommon();


    QVector<QString*>* lstWordCommon;
    QVector<QString*>* lstWordScrable;
    QFile* f_scrable;
    QFile* f_common;
};

#endif // WORDREADER_H
