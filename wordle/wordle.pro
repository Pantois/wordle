QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++2a

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
INCLUDEPATH += \
    include/

SOURCES += \
    src/dialogabout.cpp \
    src/formnewgame.cpp \
    src/inputletter.cpp \
    src/inputword.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/suggestionlistmodel.cpp \
    src/wordle.cpp \
    src/wordreader.cpp \
    src/appsettings.cpp

HEADERS += \
    include/dialogabout.h \
    include/formnewgame.h \
    include/inputletter.h \
    include/inputword.h \
    include/mainwindow.h \
    include/suggestionlistmodel.h \
    include/wordle.h \
    include/wordreader.h \
    include/appsettings.h

FORMS += \
    src/dialogabout.ui \
    src/formnewgame.ui \
    src/mainwindow.ui

TRANSLATIONS += \
    wordle_fr.ts \
    wordle_en.ts

CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc

DISTFILES += \
    wordle_en.ts
