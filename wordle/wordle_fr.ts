<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AppSettings</name>
    <message>
        <location filename="src/appsettings.cpp" line="57"/>
        <source>Motus</source>
        <translation>Motus</translation>
    </message>
    <message>
        <location filename="src/appsettings.cpp" line="90"/>
        <source>Translation to %1 not found</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="src/dialogabout.ui" line="14"/>
        <source>Dialog</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="src/dialogabout.ui" line="26"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:18pt;&quot;&gt;Wordle le jeu&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:18pt;&quot;&gt;Wordle le jeu&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="src/dialogabout.ui" line="60"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	Le but du jeu est de trouver le mot secret. Vous pouvez choisir la taille du mot à trouver entre 4 et 8 lettres. Le jeu peut vous aider à trouver le mot secret en affichant le nombre de mots possible restant et si vous l&apos;accepter en conseillant un mot.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	Après avoir rentrer un mot qui existe, le jeu changera la couleur des cases des lettres. &lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	- Vert : la lettre est dans le mot et au bon endroit&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	- Jaune : la lettre est dans le mot à une autre position&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	- Gris : la lettre n&apos;est pas dans le mot&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	Le but du jeu est de trouver le mot secret. Vous pouvez choisir la taille du mot à trouver entre 4 et 8 lettres. Le jeu peut vous aider à trouver le mot secret en affichant le nombre de mots possible restant et si vous l&apos;accepter en conseillant un mot.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	Après avoir rentré un mot qui existe, le jeu changera la couleur des cases des lettres. &lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	- Vert : la lettre est dans le mot et au bon endroit&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	- Jaune : la lettre est dans le mot à une autre position&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	- Gris : la lettre n&apos;est pas dans le mot&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>FormNewGame</name>
    <message>
        <location filename="src/formnewgame.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire Nouvelle partie</translation>
    </message>
    <message>
        <location filename="src/formnewgame.ui" line="48"/>
        <source>taille des mots :</source>
        <translation>taille des mots :</translation>
    </message>
    <message>
        <location filename="src/formnewgame.ui" line="61"/>
        <source>Commencer</source>
        <translation>Commencer</translation>
    </message>
    <message>
        <location filename="src/formnewgame.ui" line="74"/>
        <source>Suggestions</source>
        <translation>Suggestions</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="src/mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Fenêtre principale</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="28"/>
        <source>&amp;Jeu</source>
        <translation>&amp;Jeu</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="37"/>
        <source>&amp;Préférences</source>
        <translation>&amp;Préférences</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="41"/>
        <source>&amp;Langues</source>
        <translation>&amp;Langues</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="50"/>
        <source>&amp;Aide</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="61"/>
        <source>&amp;Nouvelle partie</source>
        <translation>&amp;Nouvelle partie</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="64"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="69"/>
        <source>&amp;Voir la réponse</source>
        <translation>&amp;Voir la réponse</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="74"/>
        <source>&amp;Quitter</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="79"/>
        <source>Français</source>
        <translation>Fançais</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="84"/>
        <source>Anglais</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="src/mainwindow.ui" line="89"/>
        <source>&amp;A propos</source>
        <translation>&amp;A propos</translation>
    </message>
</context>
<context>
    <name>Wordle</name>
    <message>
        <location filename="src/wordle.cpp" line="43"/>
        <source>Recommencer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/wordle.cpp" line="46"/>
        <source>Nouvelle partie</source>
        <translation>Nouvelle partie</translation>
    </message>
    <message>
        <location filename="src/wordle.cpp" line="160"/>
        <source>Bravo tu as réussi à trouver le mot : </source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/wordle.cpp" line="160"/>
        <source>Dommage tu n&apos;as pas réussi à trouver le mot : </source>
        <translation></translation>
    </message>
</context>
</TS>
