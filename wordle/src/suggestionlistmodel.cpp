#include <suggestionlistmodel.h>

#include <iostream>

SuggestionListModel::SuggestionListModel(QObject *parent)
    : QAbstractListModel(parent),
      lst(new QList<QPair<QString, int>*>()),
      lstAll(new QList<QPair<QString, int>*>()),
      prefix("")
{

}



void SuggestionListModel::insert(QPair<QString, int>& value){
    QList<QPair<QString, int>*>::iterator it =  std::lower_bound(this->lstAll->begin(), this->lstAll->end(), value, [](QPair<QString,int>* const & s2, const QPair<QString, int>& s1 ) -> bool { return s1.second < s2->second; });
    insertAt(it-lstAll->begin(), value);
}

void SuggestionListModel::reset(bool resetAll)
{
    if (resetAll){
        delete lstAll;
        lstAll = new QList<QPair<QString, int>*>();
    }
    // delete lstAll; // if returnKey
    int l = lst->length();
    delete lst;
    lst = new QList<QPair<QString, int>*>();
    emit rowsRemoved(this->index(0), 0, l);
}

void SuggestionListModel::displayBegin(QString &str, int sizeWord)
{
    reset();

    for (auto x = lstAll->cbegin(); x!=lstAll->cend(); ++x){
        if ((**x).first.startsWith(str)){
            lst->push_back(new QPair<QString, int>(**x));
        }
    }

    emit rowsInserted(this->index(0), 0, lst->length());
}

void SuggestionListModel::insertAt(int row, QPair<QString, int>& value){
    if (row >= 0 && row <= lst->length()){
        lstAll->insert(row, new QPair<QString, int>(value));
        if (value.first.startsWith(prefix)){
            QList<QPair<QString, int>*>::iterator it =  std::lower_bound(this->lst->begin(), this->lst->end(), value, [](QPair<QString,int>* const & s2, const QPair<QString, int>& s1 ) -> bool { return s1.second < s2->second; });
            int newRow(it - lst->begin());
            lst->insert(newRow, new QPair<QString, int>(value));
            emit rowsInserted(this->index(0), newRow, newRow); // TODO
        }

    }
}

int SuggestionListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;
    return lst->length();
}


QVariant SuggestionListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || (role != DISPLAY_ROLE && role != ELIMINATION_ROLE))
        return QVariant();
    if (index.column()==0 && index.row() < this->rowCount() && index.row() >= 0)
    {
        if (role == DISPLAY_ROLE)
            return QVariant(lst->at(index.row())->first);
        else{
            return QVariant(lst->at(index.row())->second);
        }
    }
    return QVariant();
}

