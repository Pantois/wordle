#include <appsettings.h>
#include <QDebug>

#include <iostream>


const QLocale AppSettings::defaultLocale(QLocale::system());
const QString AppSettings::languageKey("lang");
const QString AppSettings::defaultSizeKey("defaulSize");
const size_t AppSettings::defaultSize(5);
const QString AppSettings::suggestionKey("suggestionKey");
const bool AppSettings::suggestionDefault(false);



size_t AppSettings::getLength_default() const
{
    return length_default;
}

void AppSettings::setLength_default(size_t newLength_default)
{

    if (newLength_default > 3 && newLength_default < 9){
        settings.setValue(defaultSizeKey, QVariant::fromValue(newLength_default));
        length_default = newLength_default;
    }
}


const QTranslator &AppSettings::getTranslator() const
{
    return translator;
}


QApplication *AppSettings::getApp() const
{
    return app;
}



bool AppSettings::getSuggestions() const
{
    return suggestions;
}

void AppSettings::setSuggestions(bool newSuggestions)
{
    settings.setValue(suggestionKey, QVariant(newSuggestions));
    suggestions = newSuggestions;
}

AppSettings::AppSettings(QApplication *app) :
    QObject(app),
    applicationName(tr("Motus")),
    settings("Antoine Haslé", applicationName),
    translator(),
    language(),
    length_default(settings.value(defaultSizeKey, QVariant::fromValue(defaultSize)).toUInt()),
    suggestions(settings.value(suggestionKey, QVariant::fromValue(suggestionDefault)).toBool()),
    app(app)
{
    QLocale v = settings.value(languageKey, defaultLocale).toLocale();
    setLanguage(v);
}


QLocale AppSettings::getLanguage() const
{
    return *language;
}


void AppSettings::setLanguage(const QLocale & language){
    delete this->language;
    this->language = new QLocale(language);
    QString localeStr = ":/wordle_" + this->language->name().mid(0,2) + ".qm";
    translator.load(localeStr);

    settings.setValue(languageKey, *(this->language));

    if (!translator.isEmpty())
    {
        app->installTranslator(&translator);
    }
    else
    {
        qDebug() << tr("Translation to %1 not found").arg(localeStr);
    }
}


AppSettings::~AppSettings(){

}




