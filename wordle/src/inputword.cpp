#include <inputword.h>

#include <iostream>

InputWord::InputWord(QWidget *parent, const size_t s, state_input_word wordState)
    : QWidget(parent),
      sizeWord(s),
      wordState(wordState),
      layout(new QHBoxLayout()),
      currentLetter(-1),
      letters(new QVector<InputLetter*>(s, nullptr))
{
    if (sizeWord>8 || sizeWord < 4){
        exit(3);
    }
    if (wordState == DONE){
        std::cerr << "InputWord::InputWord invalid state_input_word given" << std::endl;
        exit(2);
    }
    state_input_letter state = INPUT_LETTER;
    for (auto x = letters->begin(); x!= letters->end(); ++x){
        *x = new InputLetter(nullptr, state);
        layout->addWidget(*x);
    }
    this->setLayout(layout);

}

QString InputWord::getWord(){
    QString res = "";
    for (auto x =0; x <= this->currentLetter; ++x){
        res = res + (this->letters->at(x)->getLetter()!=nullptr?*this->letters->at(x)->getLetter():QString(""));
    }
    return res;
}


bool InputWord::isComplete(){
    return currentLetter == static_cast<int>(sizeWord - 1);
}


void InputWord::setstate_input_word(state_input_word siw){
    if (this->wordState == DONE || siw == WAITING){
        exit(3);
    }
    else if (siw == DONE){
        if (this->wordState == IN_PROGRESS && currentLetter == static_cast<int>(sizeWord-1)){
            this->wordState = DONE;
        }
        else{
            exit(3);
        }
    }
    else{
        // siw == IN_PROGRESS
        if (this->wordState == WAITING){
            this->wordState = IN_PROGRESS;
        }
        else{
            exit(3);
        }
    }
}

QVector<state_input_letter> InputWord::getStateInputLetters()
{
    QVector<state_input_letter> res(sizeWord);
    for (int i = 0; i < static_cast<int>(sizeWord) ; ++i){
        res.replace(i,this->letters->at(i)->getSil());
    }
    return res;
}

void InputWord::showAnswer(QVector<state_input_letter>* sil){
    if (currentLetter != static_cast<int>(sizeWord-1)){
        exit(3);
    }
    for (size_t x = 0; x < sizeWord; ++x){
        letters->at(x)->setStateInputLetter(sil->at(x));
    }
}

void InputWord::letterEntry(const QChar c){
    if (currentLetter != static_cast<int>(sizeWord-1)){
        ++currentLetter;
        letters->at(currentLetter)->setLetter(c);
    }
}


InputWord::~InputWord(){
    for(auto x = letters->begin(); x != letters->end(); ++x){
        delete *x;
    }
    delete letters;
}


void InputWord::removeLetter(){
    if (currentLetter>=0){
        letters->at(currentLetter)->setText("");
        --currentLetter;
    }
}

