#include <formnewgame.h>
#include "ui_formnewgame.h"

#include <iostream>

FormNewGame::FormNewGame(QWidget *parent, size_t s, bool suggestions) :
    QWidget(parent),
    ui(new Ui::FormNewGame),
    length(s),
    suggestions(suggestions)
{
    ui->setupUi(this);
    ui->checkBox->setChecked(suggestions);
    ui->horizontalSlider->setSliderPosition(s);
}

FormNewGame::~FormNewGame()
{
    delete ui;
}

void FormNewGame::on_pushButton_clicked()
{
    emit newGame(length, suggestions);
}


void FormNewGame::on_horizontalSlider_actionTriggered(int)
{
    length = ui->horizontalSlider->sliderPosition();
}


void FormNewGame::on_checkBox_stateChanged(int arg1)
{
    suggestions = arg1;
}

