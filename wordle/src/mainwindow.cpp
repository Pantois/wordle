#include <mainwindow.h>
#include "ui_mainwindow.h"
#include <dialogabout.h>
#include <iostream>

MainWindow::MainWindow(QWidget *parent, QApplication * app)
    : QMainWindow(parent),
      wordle(nullptr),
      settings(new AppSettings(app)),
      fng(new FormNewGame(this, settings->getLength_default(), settings->getSuggestions()))
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setCentralWidget(fng);
    connect(fng, SIGNAL(newGame(int,bool)), this, SLOT(newGame(int,bool)));
    connect(this, SIGNAL(languageChanged(const QLocale)), settings , SLOT(setLanguage(const QLocale)));

}

MainWindow::~MainWindow()
{
    delete wordle;
    delete ui;
}


void MainWindow::on_actionNouvelle_partie_triggered()
{
    this->setCentralWidget(fng);
    delete wordle;
}


void MainWindow::on_actionVoir_la_r_ponse_triggered()
{

}


void MainWindow::on_actionQuitter_triggered()
{
    QApplication::quit();
}


void MainWindow::on_actionFran_ais_triggered()
{
    emit languageChanged(QLocale(QLocale::French));
}


void MainWindow::on_actionAnglais_triggered()
{
    emit languageChanged(QLocale(QLocale::English));
}


void MainWindow::on_actionA_propos_triggered()
{
    DialogAbout d(nullptr);
    d.exec();
}

void MainWindow::keyPressEvent(QKeyEvent* event){
    if (wordle != nullptr){
        wordle->keyPressEvent(event);
    }
}


void MainWindow::statusBar(QString str){
    ui->statusbar->showMessage(str);
}


void MainWindow::newGame(int l, bool b){
    if (static_cast<size_t>(l) != settings->getLength_default())
        settings->setLength_default(l);
    if (b != settings->getSuggestions())
        settings->setSuggestions(b);


    wordle = new Wordle(this, l, b, new QLocale(settings->getLanguage()));
    disconnect(fng, SIGNAL(newGame(int,bool)), this, SLOT(newGame(int,bool)));

    connect(wordle, SIGNAL(updateStatusBar(QString)),
            this, SLOT(statusBar(QString)));
    connect(wordle, SIGNAL(newGame(bool)), this, SLOT(newSetUp(bool)));
    this->setCentralWidget(wordle);
    delete fng;
}

void MainWindow::newSetUp(bool sameProperties){
    disconnect(wordle, SIGNAL(updateStatusBar(QString)),
            this, SLOT(statusBar(QString)));
    disconnect(wordle, SIGNAL(newGame(bool)), this, SLOT(newSetUp(bool)));

    if (sameProperties){
        QLocale * l = new QLocale(settings->getLanguage());
        Wordle * newWordle = new Wordle(this, settings->getLength_default(), settings->getSuggestions(), l); // base value
        this->setCentralWidget(newWordle);
        delete wordle;
        wordle = newWordle;
        connect(wordle, SIGNAL(updateStatusBar(QString)),
            this, SLOT(statusBar(QString)));
        connect(wordle, SIGNAL(newGame(bool)), this, SLOT(newSetUp(bool)));
    }
    else{
        fng = new FormNewGame(this, settings->getLength_default(), settings->getSuggestions());
        this->setCentralWidget(fng);
        connect(fng, SIGNAL(newGame(int,bool)), this, SLOT(newGame(int,bool)));
        delete wordle;
    }
}

