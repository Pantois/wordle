#include <wordreader.h>

#include <iostream>

#include <QLocale>

WordReader::WordReader(size_t s, QLocale* l):
    lstWordCommon(new QVector<QString*>()),
    lstWordScrable(new QVector<QString*>()),
    f_scrable(new QFile(nameFile(SCRABLE,s, l))),
    f_common(new QFile(nameFile(COMMON, s, l)))
{
    if (! f_scrable->exists() && ! f_common->exists()){
        std::cerr << "WordReader::randWord:: An error occured" << std::endl;
        exit(4);
    }
    extractWordFromFileScrable();
}



WordReader::~WordReader(){
    f_scrable->close();
    f_common->close();
    delete f_scrable;
    delete f_common;
    for (auto x = lstWordScrable->begin(); x != lstWordScrable->end(); ++x){
        delete *x;
    }
    delete lstWordScrable;
    for (auto x = lstWordCommon->begin(); x != lstWordCommon->end(); ++x){
        delete *x;
    }
    delete lstWordCommon;
}






bool WordReader::WordInFile(QString str){
    for (auto x = lstWordScrable->cbegin(); x!= lstWordScrable->cend(); ++x){
        if ((**x) == str){
            return true;
        }
    }
    return false;
}




QString WordReader::randWord(){
    extractWordFromFileCommon();
    srand(static_cast<unsigned int>(time(NULL)));
    return *lstWordCommon->at(rand()%lstWordCommon->length());
}




QVector<QString*>* WordReader::extractWordFromFileScrable(){

    bool res = f_scrable->open(QIODevice::ReadOnly);
    if (res){
        extractWordFromFile(f_scrable, lstWordScrable);
        return lstWordScrable;
    }
    return nullptr;
}

QVector<QString*>* WordReader::extractWordFromFileCommon(){

    bool res = f_common->open(QIODevice::ReadOnly);
    if (res){
        extractWordFromFile(f_common, lstWordCommon);
        return lstWordCommon;
    }
    return nullptr;
}


void WordReader::extractWordFromFile(QFile* f, QVector<QString*>* lst){
    for (QString line = QString::fromLatin1(f->readLine(16));
         !line.isNull();
         line = QString::fromLatin1(f->readLine(16))){
        if (line.contains('\n'))
            line.remove('\n');
        if (line.contains('\r'))
            line.remove('\r');
        lst->append(new QString(line.toUpper()));
    }
}





QString WordReader::nameFile(type_file tf, size_t s, QLocale* loc){
    return QString(":words/"+ loc->name().mid(0, 2)+"_"+ QString::number(s)+"_"+ QString(COMMON==tf?"common":"scrable")+".txt");
}

QVector<QString*>* WordReader::getExtractWordFromFileScrable(){
    return lstWordScrable;
}
