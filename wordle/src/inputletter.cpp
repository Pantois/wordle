#include <inputletter.h>

#include <iostream>

InputLetter::InputLetter(QWidget* parent, state_input_letter sil, QChar* v):
    QLabel(parent),
    sil(sil),
    value(v),
    cbg(getBackGroundColor(sil)),
    ctext(new QColor(Qt::black))
{
    if (value == nullptr && sil != INPUT_LETTER){
        std::cerr << "No QChar Value for an no modifiable text" << std::endl;
        exit(1);
    }
    if (sil != INPUT_LETTER){
        this->setText(*value);
    }
    setAutoFillBackground(true);
    updateColor();
    show();
    setFixedSize(30,30);
    this->setAlignment(Qt::AlignCenter);
}

QColor* InputLetter::getBackGroundColor(state_input_letter &nsil){
    QColor* color;
    switch (nsil) {
        case INPUT_LETTER:
            color = new QColor(255,255,255);
            break;
        case GREEN_LETTER:
            color = new QColor(0,255,0);
            break;
        case GREY_LETTER:
            color = new QColor(190,190,190);
            break;
        case YELLOW_LETTER:
            color = new QColor(255,255,0);
            break;
    }
    return color;
}



void InputLetter::setStateInputLetter(state_input_letter nsil){
    if (sil!= nsil && sil == INPUT_LETTER){
        delete cbg;
        this->sil = nsil;
        cbg = getBackGroundColor(sil);
        updateColor();
    }
}

void InputLetter::updateColor(){
    QPalette p = palette(); // define pallete for textEdit..
    p.setColor(QPalette::Window, *cbg); // set color "Red" for textedit base
    p.setColor(QPalette::Text, *ctext); // set text color which is selected from color pallete
    setPalette(p);
}

state_input_letter InputLetter::getSil() const
{
    return sil;
}

int InputLetter::silToInt(QVector<state_input_letter> sil)
{
    int i = std::max(std::max(std::max(INPUT_LETTER, GREEN_LETTER), GREY_LETTER), YELLOW_LETTER);
    int res =0;
    for (auto x = sil.cbegin(); x != sil.cend(); ++x){
        res *= i;
        res += *x;
    }
    return res;
}

void InputLetter::setLetter(const QChar& c){
    delete value;
    value = new QChar(c);
    this->setText(c);
}

InputLetter::~InputLetter(){
    delete value;
    delete cbg;
    delete ctext;
}


QChar* InputLetter::getLetter(){
    return value;
}
