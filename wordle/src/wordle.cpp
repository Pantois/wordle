#include <wordle.h>
#include <iostream>
#include <unistd.h>
#include <iostream>
#include <QKeyEvent>

#include <wordreader.h>

Wordle::Wordle(QWidget *parent, size_t sizeWord, bool suggestions, QLocale* l)
    : QWidget{parent},
    inputWords(new QVector<InputWord*>(nb_word, nullptr)),
    layout(new QVBoxLayout()),
    currentWord(0),
    suggestion(suggestions),
    sizeWord(sizeWord),
    wr(new WordReader(sizeWord, l)),
    secretWord(generateSecretWord(sizeWord)),
    label(new QLabel(this)),
    buttonRetry(new QPushButton(this)),
    buttonNewGame(new QPushButton(this)),
    model(new SuggestionListModel()),
    view(new QListView(this)),
    lstPlayedWord(nb_word),
    lstAnswer(nb_word),
    lstLastWord(new QVector<QString*>(*wr->getExtractWordFromFileScrable())),
    lstGreyChar(26, false),
    lstGreenChar(sizeWord, ' '),
    lstYellow(26, false),
    lstFreq(calcLstFreq(lstLastWord))
{

    bool test = true;
    for (auto x = inputWords->begin(); x!= inputWords->end(); ++x){
        *x = new InputWord(nullptr, sizeWord, test?IN_PROGRESS:WAITING);
        layout->addWidget(*x);
        test = false;
    }
    QHBoxLayout* layoutH = new QHBoxLayout();

    label->setVisible(false);

    buttonRetry->setVisible(false);
    buttonRetry->setText(tr("Recommencer"));

    buttonNewGame->setVisible(false);
    buttonNewGame->setText(tr("Nouvelle partie"));

    layout->addWidget(label);
    layout->addWidget(buttonRetry);
    layout->addWidget(buttonNewGame);
    if (suggestion){
        view->setFocusPolicy(Qt::NoFocus);
        view->setFixedWidth(200);
        layoutH->addWidget(view);
    }
    else{
        view->setVisible(false);
    }

    layoutH->addLayout(layout);



    if (suggestion){
        view->setModel(model);
        view->setModelColumn(0);
    }

    connect(buttonNewGame, SIGNAL(pressed()), this, SLOT(newGame()));
    connect(buttonRetry, SIGNAL(pressed()), this, SLOT(retry()));


    this->setLayout(layoutH);

    connect(this, SIGNAL(addLetter(QChar)),
            inputWords->at(currentWord), SLOT(letterEntry(const QChar)));
    connect(this, SIGNAL(sendAnswer(QVector<state_input_letter>*)),
            inputWords->at(currentWord), SLOT(showAnswer(QVector<state_input_letter>*)));
    connect(this, SIGNAL(removeLetter()), inputWords->at(currentWord), SLOT(removeLetter()));
    if (suggestion)
        getSuggestWord();
}



Wordle::~Wordle(){
    for (auto x = inputWords->begin(); x != inputWords->end(); ++x){
        delete *x;
    }
    delete inputWords;
    delete wr;
    delete label;
    delete buttonNewGame;
    delete buttonRetry;
}


bool Wordle::isLastWord(){
    return currentWord == nb_word-1;
}


QVector<state_input_letter>* Wordle::getAnswer(QString value, QString str){
    bool test = str == QString("");
    if (str == QString("")){
        str = secretWord;
    }
    if (value == QString(""))
        value = this->inputWords->at(currentWord)->getWord();
    if (isWord(value)){
        QVector<state_input_letter>* res(new QVector<state_input_letter>(sizeWord, GREY_LETTER));
        QMap<QChar, int> l;

        for (auto x = str.cbegin(); x!= str.cend(); ++x){
            if (l.contains(*x)){
                l[*x] = l[*x]+1;
            }
            else {
                l.insert(*x, 1);
            }
        }

        for(unsigned long x = 0; x < sizeWord ; ++x){
            if (str.at(x).toUpper() == value.at(x)){
                res->replace(x, GREEN_LETTER);
                l[str.at(x).toUpper()] = l[str.at(x).toUpper()] - 1;
            }
        }

        for (unsigned long x = 0; x < sizeWord ; ++x){

            if (res->at(x) == GREY_LETTER &&  str.contains(value.at(x))){
                if (l[value.at(x)]!=0){
                    res->replace(x, YELLOW_LETTER);
                    l[value.at(x)] = l[value.at(x)] - 1;
                }

            }
        }
        if (test)
            for (unsigned long x = 0; x < sizeWord ; ++x){
                if (res->at(x) == GREY_LETTER){
                    lstGreyChar.replace(str.at(x).toLatin1()-'A', true);
                }
                else if (res->at(x) == GREEN_LETTER){
                    lstGreenChar.replace(x, str.at(x));
                }
                else{
                    lstYellow.replace(str.at(x).toLatin1()-'A', true);
                }
            }
        return res;
    }
    return nullptr;
}


void Wordle::showAnswer(bool success){
    // TODO
    label->setText(QString(success?tr("Bravo tu as réussi à trouver le mot : "):tr("Dommage tu n'as pas réussi à trouver le mot : ")) + secretWord);
    label->setVisible(true);
    buttonNewGame->setVisible(true);
    buttonRetry->setVisible(true);
}


size_t Wordle::getNumberAvaillableWord(){
    int x = 0;
    while (x < lstLastWord->length()){
        auto answer = lstAnswer.at(currentWord-1);
        auto played = lstPlayedWord.at(currentWord-1);
        bool resBool = verifyFilterNaive(*lstLastWord->at(x), answer, played);
        if (!resBool){
            lstLastWord->remove(x);
        }
        else {
            ++x;
        }

    }
    lstFreq = calcLstFreq(lstLastWord);
    return lstLastWord->length();
}


void Wordle::getSuggestWord(){
    this->setCursor(Qt::WaitCursor);
    model->reset();
    QVector<QString*>* l = wr->getExtractWordFromFileScrable();
    for (auto x = l->begin(); x!= l->end(); ++x){
        QPair<QString,int> p(**x, getWordScoreToFind(**x));
        model->insert(p);
    }
    this->setCursor(Qt::ArrowCursor);
}


void Wordle::getSuggestWord(QString str){
    model->displayBegin(str, sizeWord);
}


void Wordle::keyPressEvent(QKeyEvent* event){
    if (event->key() >= 65 && event->key() <= 90){
        emit addLetter(event->text().at(0).toUpper());
        if (suggestion)
            getSuggestWord(this->inputWords->at(this->currentWord)->getWord());
    }
    else if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return){
        if (inputWords->at(currentWord)->isComplete()){
            bool is_finished = true;
            auto res = getAnswer();
            if (getAnswer()==nullptr){
                // TODO not a word

                return ;
            }
            for(auto x = res->cbegin(); x!= res->cend(); ++x){
                if (*x != GREEN_LETTER){
                    is_finished = false;
                    break;
                }
            }
            lstAnswer.replace(currentWord, *res);
            lstPlayedWord.replace(currentWord, inputWords->at(currentWord)->getWord());
            emit sendAnswer(res);
            // disconnect with current word
            if (currentWord != nb_word){
                disconnect(this, SIGNAL(addLetter(QChar)),
                        inputWords->at(currentWord), SLOT(letterEntry(QChar)));
                disconnect(this, SIGNAL(sendAnswer(QVector<state_input_letter>*)),
                        inputWords->at(currentWord), SLOT(showAnswer(QVector<state_input_letter>*)));
                disconnect(this, SIGNAL(removeLetter()), inputWords->at(currentWord), SLOT(removeLetter()));
                ++currentWord;
            }

            // connect with the new current word
            if (currentWord != nb_word && !is_finished){
                connect(this, SIGNAL(addLetter(QChar)),
                        inputWords->at(currentWord), SLOT(letterEntry(QChar)));
                connect(this, SIGNAL(sendAnswer(QVector<state_input_letter>*)),
                        inputWords->at(currentWord), SLOT(showAnswer(QVector<state_input_letter>*)));
                connect(this, SIGNAL(removeLetter()), inputWords->at(currentWord), SLOT(removeLetter()));
            }
            // dispay the answer
            if (is_finished || currentWord == nb_word){
                showAnswer(is_finished);
            }
            else{
                model->reset(true);
                if (suggestion)
                    getSuggestWord();
            }
            emit updateStatusBar(QString("Nombre de mots possible restant : ")+QString::number(getNumberAvaillableWord()));
        }
    }
    else if (event->key() == 16777219){// correspond to the remove key
        emit removeLetter();
        if (suggestion)
            getSuggestWord(this->inputWords->at(this->currentWord)->getWord());
    }
}

bool Wordle::verifyFilterNaive(QString &str, QVector<state_input_letter> &f, QString& vRes)
{
    return ((*this->getAnswer(QString(vRes), QString(str)))== f);

}

bool Wordle::verifyFilter(QString &str, QVector<state_input_letter> &f, QString &vRes)
{
    QVector<bool> treated(sizeWord, false);

    for (int x = 0 ; x< str.length(); ++x){
        if (f.at(x) == GREEN_LETTER){
            treated.replace(x, true);
            if (str.at(x) != vRes.at(x))
                return false;
        }
    }

    for (int x = 0 ; x < str.length(); ++x){
        if (!treated.at(x)){
            if (f.at(x) == YELLOW_LETTER){
                if (!str.contains(vRes.at(x))){
                    return false;
                }
                else{
                    int n = str.count(vRes.at(x));
                    int c = 0;
                    int b = 0;
                    int total = 0;
                    for (int y = 0; y < str.length(); ++y){
                        if (vRes.at(y)==vRes.at(x)){
                            ++total;
                            if (f.at(y) == YELLOW_LETTER){
                                ++c;
                            }
                            if (f.at(y) == GREEN_LETTER)
                                ++b;
                        }
                    }
                    if (total != c + b && n > c+b){
                        return false;
                    }
                }
            }
        }
    }
    return true;
}


int Wordle::getWordScoreToFind(const QString &str)
{


    for (size_t x = 0; x < currentWord; ++x){
        if (lstPlayedWord.at(x) == str){
            return -10000;
        }
    }
    auto f = [](QString  * const& s1,QString  * const& s2) -> bool {return *s2 < *s1;};


    if (lstLastWord->length()< 50){
        QVector<int> v;
        for (auto x = lstLastWord->cbegin(); x != lstLastWord->cend(); ++x){
            int elt = InputLetter::silToInt( *getAnswer(str, **x));
            if (std::find(v.cbegin(), v.cend(),elt)==v.cend()){
                v.append(elt);
            }
        }
        QString* const s = new QString(str);
        bool isPossible = std::binary_search(lstLastWord->begin(), lstLastWord->end(), s, f);
        delete s;
        return v.length()+(isPossible?10000:-10);
    }
    else {
        int res = 0;
        if (currentWord != 0)
            for (auto x = str.cbegin(); x != str.cend(); ++x){
                res += (lstYellow.at(x->toLatin1()-'A')?40:0)/*+(lstGreenChar.at(x-str.cbegin())==*x?30:0)*/-(lstGreyChar.at((*x).toLatin1()-'A')?52:0);
            }

        for (auto x = str.cbegin(); x != str.cend(); ++x){
            res += static_cast<int>(lstFreq.at((*x).toLatin1()-'A')*14.0);

        }
        QString* const s = new QString(str);
        bool isPossible = std::binary_search(lstLastWord->begin(), lstLastWord->end(), s, f);
        delete s;
        return res - numberSimilarLetter(str) + (isPossible?10000:-10);
    }
}

bool Wordle::isWord(QString & str){
    // TODO
    auto lst = wr->getExtractWordFromFileScrable();
    for (auto x = lst->cbegin(); x!= lst->cend(); ++x){
        if (**x == str){
            return true;
        }
    }
    return false;
}

QString Wordle::generateSecretWord(size_t s){
    auto str  = wr->randWord().toUpper();
    return str;
}

int Wordle::numberSimilarLetter(const QString &str)
{
    int res = 0;
    for (auto x = str.cbegin(); x!= str.cend(); ++x){
        bool test = false;
        for (auto y = x+1; y!= str.cend(); ++y){
            if (*y == *x){
                if (test){
                    res+=20;
                }
                else
                    res+=3;
                test = true;
            }
        }
    }
    return res;
}

QVector<double> Wordle::calcLstFreq(QVector<QString *> *lstWord)
{
    QVector<double> res = QVector<double>(26, 0);
    for (auto x = lstWord->cbegin(); x != lstWord->cend(); ++x){
        for (auto y = (*x)->cbegin(); y!= (*x)->cend(); ++y){
            res.replace(y->toLatin1()-'A',res.at(y->toLatin1()-'A')+1);
        }
    }
    double div = lstWord->length() * lstWord->first()->length();
    for (auto x = res.begin(); x != res.end(); ++x){
        *x = (*x) / div;
    }

    return res;
}

void Wordle::retry(){
    emit newGame(true);
}




void Wordle::newGame(){
    emit newGame(false);
}





