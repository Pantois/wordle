# wordle

Ce projet est fait dans le cadre du cours de langage objet avancé (LOA). Il consiste à créer un jeu du type Motus/Wordle en y ajoutant des suggestions pour aider l'utilisateur. 

## Cahier des charges

### fonctionnalités principales
* Wordle : Choix de la taille des mots
* accepte tous les mots du scrable
* fait deviné uniquement les mots les plus courrant
* Choix de la taille et de la langue du mot à deviner
* tableau des scores

### fonctionnalités annexes

* status bar : nombre de mots restant
### fonctionnalités concrêtes
* révéler la réponse
* tapper un mot et le tester
* partager mes résultats
* voir le tableau des scores

### modèle
* liste de mots qui élimine le plus de mots en un essai en moyenne

## Règle à préciser 
* accents 
* lettres apparaissant plusieurs fois